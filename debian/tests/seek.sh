#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

set -e

echo "This is my test" > /tmp/fortune

cd debian/tests/images-for-seek

seek_script || exit 77
